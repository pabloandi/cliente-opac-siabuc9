# -*- coding: utf-8 -*-
import sys, ConfigParser as cfgparser, os, MySQLdb as mysql, psycopg2 as pg, re, types, datetime, logging as log, warnings
from decimal import Decimal
from time import time

config=cfgparser.RawConfigParser()
config.read('../config.ini')

log.basicConfig(format='%(asctime)s -> %(levelname)s: %(message)s',filename='cliente-siabuc9.log',level=log.INFO)
log.captureWarnings(True)


inicio=time()
log.debug("******** iniciando proceso de prestamos ***********")

try:
    connSiabuc = pg.connect(host=config.get('hostsiabuc','host'), user=config.get('hostsiabuc','user'), password=config.get('hostsiabuc','password'), database=config.get('hostsiabuc','database'), port=int(config.get('hostsiabuc','port')))
    connSiabuc.set_client_encoding(config.get('hostsiabuc','codificacion'))
    siabuc=connSiabuc.cursor()
except pg.InternalError, e:
    log.critical("ERROR en la conexion con siabuc -> %s " % e)
    sys.exit (1)

try:
  connSbweb=mysql.connect(host=config.get('hostopac','host'), user=config.get('hostopac','user'),passwd=config.get('hostopac','password'),db=config.get('hostopac','database'),port=int(config.get('hostopac','port')))
  connSbweb.set_character_set('utf8')
  opac=connSbweb.cursor()
except mysql.Error, e:
  log.critical("Error en la conexion con el opac -> %s: %s" % (e.args[0], e.args[1]))
  sys.exit (1)

##truncar las tablas

try:
    tablas=["PrestamosGeneral", "Reservados", "MultasPendientes"]
    for tabla in tablas:
       opac.execute( """TRUNCATE TABLE %s """ % tabla)
       log.debug("truncando tabla %s " % tabla)
except opac.Error, e:
    log.critical("ERROR no se pudo truncar la tabla %s : %s"  % (e.args[0], e.args[1]))
    sys.exit (1)

##prestamos general
log.debug("******* ingresando datos en la tabla Prestamos *******")

try:
    siabuc.execute("""
    SELECT 
        no_cuenta, 						--0
        no_adqui, 						--1
        usuario, 						--2
        titulo,							--3
        autor,							--4
        clasificacion,					--5
        f_prestamo AS fecha_salida,		--6
        f_entrega AS fecha_entrega,		--7
        'Prestamo' AS tipo_prestamo, 	--8
        idbiblioteca as Biblioteca		--9
    FROM
      prestamo.prestamos
    WHERE
      tipo_prestamo<>'PS'
    UNION
    SELECT
      no_cuenta,							--0
      no_adqui,								--1
      usuario,								--2
      titulo,								--3
      autor,								--4
      clasificacion,						--5
      NULL AS fecha_salida,					--6
      NULL AS fecha_entrega,				--7
      'Prestamo interno' AS tipo_prestamo, 	--8
      idbiblioteca as Biblioteca			--9
    FROM
      prestamo.prestamos
    WHERE
      tipo_prestamo='PS'
    """)
    
    opac.executemany("""
        REPLACE INTO PrestamosGeneral 
        (
            NoCuenta,
            NumAdqui,
            Nombre,
            Titulo,
            Autor,
            Clasificacion,
            FechaSalida,
            FechaEntrega,
            TipoPrestamo,
            Biblioteca
        ) 
        VALUES 
        (
            %s,
            %s,
            %s,
            %s,
            %s,
            %s,
            %s,
            %s,
            %s,
            %s
    )
    """ ,siabuc.fetchall())
except pg.ProgrammingError, e:
  log.critical("ERROR no se pudo ejecutar la seleccion de prestamos en siabuc -> %s " % e)
  sys.exit (1)
except opac.Error, e:
  log.critical("ERROR no se pudo ejecutar el ingreso de datos de prestamos -> %s : %s" % (e.args[0], e.args[1]))
  sys.exit (1)
except UnicodeError, e:
  warnings.warn("error de codificacion -> %s : %s" % (e.args[0], e.args[1]), UnicodeWarning)

##reservaciones	
log.debug("******* ingresando datos en la tabla Reservaciones *******")

try:
    siabuc.execute("""
    SELECT 
        no_adqui,
        no_cuenta,
        fecha_inicio,
        fecha_fin,
        idbiblioteca as biblioteca
    FROM 
        prestamo.reservaciones
    WHERE 
        activa=true
    """)
    
    
    opac.executemany("""
        REPLACE INTO 
        Reservados 
        (
            numadqui, 
            NoCuenta, 
            fechainicial, 
            fechafinal, 
            Biblioteca 
        ) 
        VALUES 
        (
            %s,
            %s,
            %s,
            %s,
            %s
        )
    """,siabuc.fetchall())
except pg.ProgrammingError, e:
  log.critical("ERROR no se pudo ejecutar la seleccion de grupos en siabuc -> %s" % e)
  sys.exit (1)
except opac.Error, e:
  log.critical("ERROR no se pudo ejecutar el ingreso de datos de grupos -> %s : %s" % (e.args[0], e.args[1]))
  sys.exit (1)
except UnicodeError, e:
  warnings.warn("error de codificacion -> %s : %s" % (e.args[0], e.args[1]), UnicodeWarning)

##multas_pendientes	
log.debug("******* ingresando datos en la tabla Multas pendientes *******")
try:
    siabuc.execute("""
    SELECT 
      pb.idbloqueo as idbloqueo,
      pb.no_cuenta as no_cuenta,
      cu.nombre as nombre,
      pb.f_devolucion as f_devolucion,
      pb.monto as monto,
      pb.observaciones as observaciones,
      pb.analista as analista,
      pb.idbloqueo as idbloqueo
    FROM 
        prestamo.bloqueos pb JOIN
        catalogos.usuarios cu ON pb.no_cuenta=cu.no_cuenta
    """)
    
    
    opac.executemany("""
        REPLACE INTO 
        MultasPendientes 
        (
            IdMulta, 
            NoCuenta, 
            Nombre, 
            FechaDevolucion, 
            Monto, 
            Observaciones, 
            idCapturista, 
            idBloqueo
        ) 
        VALUES 
        (
            %s,
            %s,
            %s,
            %s,
            %s,
            %s,
            %s,
            %s
        )
    """,siabuc.fetchall())
except pg.ProgrammingError, e:
  log.critical("ERROR no se pudo ejecutar la seleccion de multas pendientes en siabuc -> %s " % e)
  sys.exit (1)
except opac.Error, e:
  log.critical("ERROR no se pudo ejecutar el ingreso de datos de multas pendientes -> %s : %s" % (e.args[0], e.args[1]))
  sys.exit (1)
except UnicodeError, e:
  warnings.warn("error de codificacion -> %s : %s" % (e.args[0], e.args[1]), UnicodeWarning)

fin=time()
log.info("Proceso de prestamos realizado en %s segundos " % (fin-inicio))
