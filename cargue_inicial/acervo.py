# -*- coding: utf-8 -*-
import sys, ConfigParser as cfgparser, os, MySQLdb as mysql, psycopg2 as pg, re, types, datetime, logging as log, warnings
from decimal import Decimal
from time import time

config=cfgparser.RawConfigParser()
config.read('../config.ini')

log.basicConfig(format='%(asctime)s -> %(levelname)s: %(message)s',filename='cliente-siabuc9.log',level=log.INFO)
log.captureWarnings(True)

inicio=time()
log.debug("******** iniciando proceso de acervo ***********")

try:
    connSiabuc = pg.connect(host=config.get('hostsiabuc','host'), user=config.get('hostsiabuc','user'), password=config.get('hostsiabuc','password'), database=config.get('hostsiabuc','database'), port=int(config.get('hostsiabuc','port')))
    connSiabuc.set_client_encoding(config.get('hostsiabuc','codificacion'))
    siabuc=connSiabuc.cursor()
except pg.InternalError, e:
  log.critical("ERROR en la conexion con siabuc -> %s " % e)
  sys.exit (1)

try:
  connSbweb=mysql.connect(host=config.get('hostopac','host'), user=config.get('hostopac','user'),passwd=config.get('hostopac','password'),db=config.get('hostopac','database'),port=int(config.get('hostopac','port')))
  connSbweb.set_character_set('utf8')
  opac=connSbweb.cursor()
except mysql.Error, e:
  log.critical("Error en la conexion con el opac -> %s: %s" % (e.args[0], e.args[1]))
  sys.exit (1)

##truncar las tablas

try:
  tablas=['EjemplaresGeneral','FichasGeneral','UsuariosGeneral','EtiquetasMarc','Bibliotecas']
  for tabla in tablas:
    opac.execute("TRUNCATE TABLE %s " % tabla)
    log.info("truncando tabla %s " % tabla)
except opac.Error, e:
    log.critical("ERROR no se pudo truncar la tabla %s -> %s : %s"  % (tabla, e.args[0], e.args[1]))
    sys.exit (1)

##ejemplares general
log.debug("******* ingresando datos en la tabla Ejemplares *******")
try:
    siabuc.execute("""
      SELECT
        ('L' || e.idficha) AS ficha_no,
        e.no_adqui AS numadqui,
        b.idbiblioteca AS biblioteca,
        NULL AS ano,
        e.volumen AS volumen,
        NULL AS numero,
        e.ejemplar AS ejemplar

      FROM
        analisis.ejemplares e, analisis.fichas f, catalogos.bibliotecas b
      WHERE
        e.idficha=f.idficha AND e.idbiblioteca=b.idbiblioteca AND f.tipoficha='L'

      UNION
      SELECT
        ('R' || e.idficha) AS ficha_no,
        e.no_adqui AS numadqui,
        b.idbiblioteca AS biblioteca,
        e.revanio AS ano,
        e.revvol AS volumen,
        e.revnum AS numero,
        e.revejemplar AS ejemplar

      FROM
        analisis.ejemplares e, analisis.fichas f, catalogos.bibliotecas b
      WHERE
        e.idficha=f.idficha AND e.idbiblioteca=b.idbiblioteca AND f.tipoficha='R'

    """ )


    opac.executemany("""
		INSERT INTO
		EjemplaresGeneral
		(
			Ficha_No,
			NumAdqui,
			Biblioteca,
			Ano,
			Volumen,
			Numero,
			Ejemplar
		)
		VALUES
		(
			%s,
			%s,
			%s,
			%s,
			%s,
			%s,
			%s
		)""",siabuc.fetchall())

    siabuc.close()
    opac.close()
except pg.ProgrammingError, e:
  log.critical("ERROR no se pudo ejecutar la seleccion de Ejemplares en siabuc -> %s " % e)
  sys.exit (1)
except opac.Error, e:
  log.critical("ERROR no se pudo ejecutar el ingreso de datos de Ejemplares -> %s : %s" % (e.args[0], e.args[1]))
  sys.exit (1)
except UnicodeError, e:
  warnings.warn("error de codificacion -> %s : %s" % (e.args[0], e.args[1]), UnicodeWarning)


##fichas general
log.debug("******* ingresando datos en la tabla Fichas *******")
try:

    siabuc=connSiabuc.cursor()
    opac=connSbweb.cursor()

    siabuc.execute("""
      SELECT
        (f.tipoficha || f.idficha) AS ficha_no,
        (array_to_string(array(SELECT (case when contenido like '%%-@@%%' then (select texto from catalogos.referencias r where idlink=r.idreferencia) when contenido like '%%@@%%' then (select contenido from catalogos.autoridades a where idlink=a.idautoridad) else contenido end) as contenido FROM analisis.etiquetasmarc e where e.idficha=f.idficha),'|')) as etiquetasmarc,
        f.titulo AS titulo,
        f.autor AS autor,
        f.clasificacion AS clasificacion,
        f.isbn AS isbn,
        (case when f.fecha1 is not null and f.fecha2 is not null then (cast(f.fecha1 || ', c' || f.fecha2 as varchar)) when f.fecha1 is not null and f.fecha2 is null then (cast(f.fecha1 as varchar)) when f.fecha1 is null and f.fecha2 is not null then (cast('c' || f.fecha2 as varchar)) end) as fechapublicacion,
        t.nombre AS tipodematerial,
        f.idbiblioteca as biblioteca
      FROM
        analisis.fichas f, catalogos.tiposmateriales t
      WHERE
        f.tipoficha='L' AND f.idtipomaterial=t.idtipomaterial

      UNION
      SELECT
        (f.tipoficha || f.idficha) AS ficha_no,
        (array_to_string(array(SELECT (case when contenido like '%%-@@%%' then (select texto from catalogos.referencias r where idlink=r.idreferencia) when contenido like '%%@@%%' then (select contenido from catalogos.autoridades a where idlink=a.idautoridad) else contenido end) as contenido FROM analisis.etiquetasmarc e where e.idficha=f.idficha),'|')) as etiquetasmarc,
        f.titulo AS titulo,
        f.autor AS autor,
        f.clasificacion AS clasificacion,
        f.isbn AS isbn,
        (case when f.fecha1 is not null and f.fecha2 is not null then (cast(f.fecha1 || ', c' || f.fecha2 as varchar)) when f.fecha1 is not null and f.fecha2 is null then (cast(f.fecha1 as varchar)) when f.fecha1 is null and f.fecha2 is not null then (cast('c' || f.fecha2 as varchar)) end) as fechapublicacion,
        'Revista' AS tipodematerial,
        f.idbiblioteca as biblioteca
      FROM
        analisis.fichas f
      WHERE
        f.tipoficha='R'

      UNION
      SELECT
        (f.tipoficha || f.idficha) AS ficha_no,
        ('|000' || (select titulo from analisis.fichas where idficha in (select idficha from analisis.holdings where idholding in (select idholding from analisis.fichas where tipoficha='A' and idficha = f.idficha))) || '|245' || f.titulo || '|100' || array_to_string(array(SELECT (aut.apellidos || ', ' || aut.nombre) FROM catalogos.autoridades aut WHERE aut.idautoridad IN (SELECT et.idlink FROM analisis.etiquetasmarc et WHERE et.idficha=f.idficha)), ',') || array_to_string(array(SELECT (case when contenido like '%%-@@%%' then (select texto from catalogos.referencias r where idlink=r.idreferencia) when contenido like '%%@@%%' then (select contenido from catalogos.autoridades a where idlink=a.idautoridad)  else contenido end) as contenido FROM analisis.etiquetasmarc e where e.idficha=f.idficha),'|')) AS etiquetasmarc,
        f.titulo AS titulo,
        f.autor AS autor,
        f.clasificacion AS clasificacion,
        f.isbn AS isbn,
        (case when f.fecha1 is not null and f.fecha2 is not null then (cast(f.fecha1 || ', c' || f.fecha2 as varchar)) when f.fecha1 is not null and f.fecha2 is null then (cast(f.fecha1 as varchar)) when f.fecha1 is null and f.fecha2 is not null then (cast('c' || f.fecha2 as varchar)) end) as fechapublicacion,
        'Articulo de revista' AS tipodematerial,
        f.idbiblioteca as biblioteca
      FROM
        analisis.fichas f
      WHERE
        f.tipoficha='A'

    """)


    opac.executemany("""
		INSERT INTO
		FichasGeneral
		(
			Ficha_No,
			EtiquetasMARC,
			Titulo,
			Autor,
			Clasificacion,
			Isbn,
			FechaPublicacion,
			TipoMaterial,
			Biblioteca
		)
		VALUES
		(
			%s,
			%s,
			%s,
			%s,
			%s,
			%s,
			%s,
			%s,
			%s
    )""",siabuc.fetchall())


    siabuc.close()
    opac.close()
except pg.ProgrammingError, e:
  log.critical("ERROR no se pudo ejecutar la seleccion de Fichas en siabuc -> %s " % e)
  sys.exit (1)
except opac.Error, e:
  log.critical("ERROR no se pudo ejecutar el ingreso de datos de Fichas -> %s : %s" % (e.args[0], e.args[1]))
  sys.exit (1)
except UnicodeError, e:
  warnings.warn("error de codificacion -> %s : %s" % (e.args[0], e.args[1]), UnicodeWarning)


##usuarios general
log.debug("******* ingresando datos en la tabla Usuarios *******")
try:

    siabuc=connSiabuc.cursor()
    opac=connSbweb.cursor()

    siabuc.execute("""
      SELECT
        u.no_cuenta,
        u.nombre,
        g.descripcion AS no_grupo,
        e.nombre AS no_escuela,
        u.correo AS email,
        u.domicilio,
        u.colonia,
        u.ciudad_estado,
        u.telefono,
        u.notas,
        (SELECT SUM(b.monto) FROM prestamo.bloqueos b WHERE b.no_cuenta=u.no_cuenta  ) AS multa,
        CASE
            WHEN (select valor from config_siabuc.configuracion where descripcion='VIGENCIA') = 'USUARIO'
                THEN u.fin_vigencia
            WHEN (select valor from config_siabuc.configuracion where descripcion='VIGENCIA') = 'GRUPO'
                THEN g.fin_vigencia
        END 
      FROM
        catalogos.usuarios u
        LEFT JOIN catalogos.grupos g ON g.no_grupo=u.no_grupo
        LEFT JOIN catalogos.escuelas e ON e.idescuela=u.idescuela

    """ )


    opac.executemany("""
		INSERT INTO
		UsuariosGeneral
		(
			NoCuenta,
			Nombre,
			NoGrupo,
			NoEscuela,
			Email,
			Domicilio,
			Colonia,
			CiudadEstado,
			Telefono,
			Notas,
			Multa,
			Vigencia
		)
		VALUES
		(
			%s,
			%s,
			%s,
			%s,
			%s,
			%s,
			%s,
			%s,
			%s,
			%s,
			%s,
			%s
	)""", siabuc.fetchall())

    siabuc.close()
    opac.close()

except pg.ProgrammingError, e:
  log.critical("ERROR no se pudo ejecutar la seleccion de Usuarios en siabuc -> %s " % e)
  sys.exit (1)
except opac.Error, e:
  log.critical("ERROR no se pudo ejecutar el ingreso de datos de Usuarios -> %s : %s" % (e.args[0], e.args[1]))
  sys.exit (1)
except UnicodeError, e:
  warnings.warn("error de codificacion -> %s : %s" % (e.args[0], e.args[1]), UnicodeWarning)

##etiquetas marc
log.debug("******* ingresando datos en la tabla EtiquetasMarc *******")
try:
    siabuc=connSiabuc.cursor()
    opac=connSbweb.cursor()
    siabuc.execute("""
		SELECT
		  idregistro,
		  et.idficha,
		  numetiqueta,
		  (case when contenido like '%%-@@%%' then (select texto from catalogos.referencias r where idlink=r.idreferencia) when contenido like '%%@@%%' then (select contenido from catalogos.autoridades a where idlink=a.idautoridad) else contenido end) as contenido,
		  posicion,
		  f.idbiblioteca as biblioteca
		FROM
		  analisis.etiquetasmarc et INNER JOIN analisis.fichas f ON f.idficha=et.idficha
		ORDER BY idficha,posicion""")

    opac.executemany("""
	  INSERT INTO
	  EtiquetasMarc
	  (
		idregistro,
		idficha,
		numetiqueta,
		contenido,
		posicion,
                biblioteca
		)
		VALUES
		(
			%s,
			%s,
			%s,
			%s,
			%s,
                        %s
		)
    """, siabuc.fetchall())
    siabuc.close()
    opac.close()
except pg.ProgrammingError, e:
  log.critical("ERROR no se pudo ejecutar la seleccion de Etiquetas marc en siabuc -> %s" % e)
  sys.exit (1)
except opac.Error, e:
  log.critical("ERROR no se pudo ejecutar el ingreso de datos de Etiquetas marc -> %s : %s" % (e.args[0], e.args[1]))
  sys.exit (1)
except UnicodeError, e:
  warnings.warn("error de codificacion -> %s : %s" % (e.args[0], e.args[1]), UnicodeWarning)

##bibliotecas
log.debug("******* ingresando datos en la tabla Bibliotecas *******")
try:
    siabuc=connSiabuc.cursor()
    opac=connSbweb.cursor()
    siabuc.execute("""
	select
		idbiblioteca,
		nombre,
		nombrecorto,
		director,
		telefono,
		correo
		from
	catalogos.bibliotecas
	""")


    opac.executemany("""
    INSERT INTO
		Bibliotecas
		(
			idbiblioteca,
			nombre,
			nombrecorto,
			director,
			telefono,
			correo,
			siabucversion,
			activo
		)
		VALUES
		(
			%s,
			%s,
			%s,
			%s,
			%s,
			%s,
			9,
			1
    )""", siabuc.fetchall())
    siabuc.close()
    opac.close()
except pg.ProgrammingError, e:
  log.critical("ERROR no se pudo ejecutar la seleccion de Bibliotecas en siabuc -> %s" % e)
  sys.exit (1)
except opac.Error, e:
  log.critical("ERROR no se pudo ejecutar el ingreso de datos de Bibliotecas -> %s : %s" % (e.args[0], e.args[1]))
  sys.exit (1)
except UnicodeError, e:
  warnings.warn("error de codificacion -> %s : %s" % (e.args[0], e.args[1]), UnicodeWarning)


fin=time()
log.info("Proceso de acervo realizado en %s segundos " % (fin-inicio))
