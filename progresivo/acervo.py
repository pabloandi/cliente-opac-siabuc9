# -*- coding: utf-8 -*-
import sys, ConfigParser as cfgparser, os, MySQLdb as mysql, psycopg2 as pg, re, types, datetime, logging as log, warnings
from decimal import Decimal
from time import time

config=cfgparser.RawConfigParser()
config.read('../config.ini')

log.basicConfig(format='%(asctime)s -> %(levelname)s: %(message)s',filename='cliente-siabuc9.log',level=log.INFO)
log.captureWarnings(True)

inicio=time()
log.debug("******** iniciando proceso de acervo ***********")

try:
    connSiabuc = pg.connect(host=config.get('hostsiabuc','host'), user=config.get('hostsiabuc','user'), password=config.get('hostsiabuc','password'), database=config.get('hostsiabuc','database'), port=int(config.get('hostsiabuc','port')))
    connSiabuc.set_client_encoding(config.get('hostsiabuc','codificacion'))
    siabuc=connSiabuc.cursor()
except pg.InternalError, e:
  log.critical("ERROR en la conexion con siabuc -> %s " % e)
  sys.exit (1)

try:
  connSbweb=mysql.connect(host=config.get('hostopac','host'), user=config.get('hostopac','user'),passwd=config.get('hostopac','password'),db=config.get('hostopac','database'),port=int(config.get('hostopac','port')))
  connSbweb.set_character_set('utf8')
  opac=connSbweb.cursor()
except mysql.Error, e:
  log.critical("Error en la conexion con el opac -> %s: %s" % (e.args[0], e.args[1]))
  sys.exit (1)


## definici�n de consultas


inserts={

"ejemplares":"""
REPLACE INTO EjemplaresGeneral VALUES (
	%s,
	%s,
	%s,
	%s,
	%s,
	%s,
	%s
)

""",

"fichas":"""

REPLACE INTO FichasGeneral VALUES (
	%s,
	%s,
	%s,
	%s,
	%s,
	%s,
	%s,
	%s,
	%s
)

""",

"usuarios":"""

REPLACE INTO UsuariosGeneral VALUES (
	%s,
	%s,
	%s,
	%s,
	%s,
	%s,
	%s,
	%s,
	%s,
	%s,
	%s,
	%s
)

""",

"etiquetas":"""

REPLACE INTO EtiquetasMarc
(
	idregistro,
	idficha,
	numetiqueta,
	contenido,
	posicion,
	biblioteca
)
VALUES
(
	%s,
	%s,
	%s,
	%s,
	%s,
	%s
)

"""

}



consultas={

"ejemplares": """

SELECT
	('L' || e.idficha) AS ficha_no,
	e.no_adqui AS numadqui,
	e.idbiblioteca AS biblioteca,
	NULL AS ano,
	e.volumen AS volumen,
	NULL AS numero,
	e.ejemplar AS ejemplar

FROM
	analisis.ejemplares e
	INNER JOIN analisis.fichas f ON e.idficha=f.idficha
	INNER JOIN catalogos.bibliotecas b ON e.idbiblioteca=b.idbiblioteca
	INNER JOIN catalogos.tiposmateriales tm ON f.idtipomaterial=tm.idtipomaterial
	INNER JOIN opac.actualizaciones a ON f.idficha::varchar = a.id_fila
WHERE
	f.tipoficha='L'
	AND (e.accesible=2 OR e.accesible=3)
	AND a.id=%s
UNION
SELECT
	('R' || e.idficha) AS ficha_no,
	e.no_adqui AS numadqui,
	e.idbiblioteca AS biblioteca,
	e.revanio AS ano,
	e.revvol AS volumen,
	e.revnum AS numero,
	e.revejemplar AS ejemplar

FROM
	analisis.ejemplares e
	INNER JOIN analisis.fichas f ON e.idficha=f.idficha
	INNER JOIN catalogos.bibliotecas b ON e.idbiblioteca=b.idbiblioteca
	INNER JOIN catalogos.tiposmateriales tm ON f.idtipomaterial=tm.idtipomaterial
	INNER JOIN opac.actualizaciones a ON f.idficha::varchar = a.id_fila
WHERE
	f.tipoficha='R'
	AND (e.accesible=2 OR e.accesible=3)
	AND a.id=%s

""",

"fichas": """

SELECT
	(f.tipoficha || f.idficha) AS ficha_no,
	regexp_replace((array_to_string(array(SELECT (case when contenido like '-@@%%' then (select texto from catalogos.referencias r where idlink=r.idreferencia) when contenido like '@@%%' then (select contenido from catalogos.autoridades a where idlink=a.idautoridad) else contenido end) as contenido FROM analisis.etiquetasmarc e where e.idficha=f.idficha),'|')), E'[\\n\\r]+', ' ', 'g' ) as etiquetasmarc,
	regexp_replace(f.titulo, E'[\\n|\\r]+', ' ', 'g' ) AS titulo,
	regexp_replace(f.autor, E'[\\n|\\r]+', ' ', 'g' ) AS autor,
	regexp_replace(f.clasificacion, E'[\\n|\\r]+', ' ', 'g' ) AS clasificacion,
	regexp_replace(f.isbn, E'[\\n|\\r]+', ' ', 'g' ) AS isbn,
	(case when f.fecha1 is not null and f.fecha2 is not null then (cast(f.fecha1 || ', c' || f.fecha2 as varchar)) when f.fecha1 is not null and f.fecha2 is null then (cast(f.fecha1 as varchar)) when f.fecha1 is null and f.fecha2 is not null then (cast('c' || f.fecha2 as varchar)) end) as fechapublicacion,
	t.nombre AS tipodematerial,
	e.idbiblioteca as biblioteca
FROM
	analisis.fichas f
	INNER JOIN analisis.ejemplares e ON e.idficha = f.idficha
	INNER JOIN catalogos.tiposmateriales t ON f.idtipomaterial=t.idtipomaterial
	INNER JOIN opac.actualizaciones a ON f.idficha::varchar = a.id_fila
WHERE
	f.tipoficha='L'
	AND e.idbiblioteca > 0
	AND a.id=%s

UNION

SELECT
	(f.tipoficha || f.idficha) AS ficha_no,
	(array_to_string(array(SELECT (case when contenido like '%%@@%%' then (select contenido from catalogos.autoridades a where idlink=a.idautoridad) else contenido end) as contenido FROM analisis.etiquetasmarc e where e.idficha=f.idficha),'|')) as etiquetasmarc,
	regexp_replace(f.titulo, E'[\\n|\\r]+', ' ', 'g' ) AS titulo,
	regexp_replace(f.autor, E'[\\n|\\r]+', ' ', 'g' ) AS autor,
	regexp_replace(f.clasificacion, E'[\\n|\\r]+', ' ', 'g' ) AS clasificacion,
	regexp_replace(f.isbn, E'[\\n|\\r]+', ' ', 'g' ) AS isbn,
	(case when f.fecha1 is not null and f.fecha2 is not null then (cast(f.fecha1 || ', c' || f.fecha2 as varchar)) when f.fecha1 is not null and f.fecha2 is null then (cast(f.fecha1 as varchar)) when f.fecha1 is null and f.fecha2 is not null then (cast('c' || f.fecha2 as varchar)) end) as fechapublicacion,
	'Revista' AS tipodematerial,
	e.idbiblioteca as biblioteca
FROM
	analisis.fichas f
	INNER JOIN analisis.ejemplares e ON e.idficha = f.idficha
	INNER JOIN opac.actualizaciones a ON f.idficha::varchar = a.id_fila
WHERE
	f.tipoficha='R'
	AND e.idbiblioteca > 0
	AND a.id=%s

UNION

SELECT
	(f.tipoficha || f.idficha) AS ficha_no,
	regexp_replace(('|000' || (select titulo from analisis.fichas where idficha in (select idficha from analisis.holdings where idholding in (select idholding from analisis.fichas where tipoficha='A' and idficha = f.idficha))) || '|245' || f.titulo || '|100' || array_to_string(array(SELECT (aut.apellidos || ', ' || aut.nombre) FROM catalogos.autoridades aut WHERE aut.idautoridad IN (SELECT et.idlink FROM analisis.etiquetasmarc et WHERE et.idficha=f.idficha)), ',') || array_to_string(array(SELECT (case when contenido like '%%@@%%' then (select contenido from catalogos.autoridades a where idlink=a.idautoridad) else contenido end) as contenido FROM analisis.etiquetasmarc e where e.idficha=f.idficha),'|')), E'[\\n\\r]+', ' ', 'g' ) AS etiquetasmarc,
	regexp_replace(f.titulo, E'[\\n|\\r]+', ' ', 'g' ) AS titulo,
	regexp_replace(f.autor, E'[\\n|\\r]+', ' ', 'g' ) AS autor,
	regexp_replace(f.clasificacion, E'[\\n|\\r]+', ' ', 'g' ) AS clasificacion,
	regexp_replace(f.isbn, E'[\\n|\\r]+', ' ', 'g' ) AS isbn,
	(case when f.fecha1 is not null and f.fecha2 is not null then (cast(f.fecha1 || ', c' || f.fecha2 as varchar)) when f.fecha1 is not null and f.fecha2 is null then (cast(f.fecha1 as varchar)) when f.fecha1 is null and f.fecha2 is not null then (cast('c' || f.fecha2 as varchar)) end) as fechapublicacion,
	'Articulo de revista' AS tipodematerial,
	e.idbiblioteca as biblioteca
FROM
	analisis.fichas f
	INNER JOIN analisis.ejemplares e ON e.idficha = f.idficha
	INNER JOIN opac.actualizaciones a ON f.idficha::varchar = a.id_fila
WHERE
	f.tipoficha='A'
	AND e.idbiblioteca > 0
	AND a.id=%s



""",

"usuarios": """

SELECT
	u.no_cuenta,
	u.nombre,
	g.descripcion AS no_grupo,
	e.nombre AS no_escuela,
	u.correo AS email,
	u.domicilio,
	u.colonia,
	u.ciudad_estado,
	u.telefono,
	u.notas,
	(SELECT SUM(b.monto) FROM prestamo.bloqueos b WHERE b.no_cuenta=u.no_cuenta  ) AS multa,
    CASE
        WHEN (select valor from config_siabuc.configuracion where descripcion='VIGENCIA') = 'USUARIO'
            THEN u.fin_vigencia
        WHEN (select valor from config_siabuc.configuracion where descripcion='VIGENCIA') = 'GRUPO'
            THEN g.fin_vigencia
    END
FROM
	catalogos.usuarios u
	INNER JOIN catalogos.grupos g ON g.no_grupo=u.no_grupo
	INNER JOIN catalogos.escuelas e ON e.idescuela=u.idescuela
	INNER JOIN opac.actualizaciones a ON a.id_fila = u.no_cuenta
WHERE
	u.no_cuenta ~* '^[0-9]+'
	AND a.id=%s
ORDER BY u.no_cuenta

""",

"etiquetas": """

SELECT
	et.idregistro as idregistro,
	et.idficha as idficha,
	et.numetiqueta as numetiqueta,
	regexp_replace(
			(case when contenido like '%%-@@%%' then
				(select texto from catalogos.referencias r where idlink=r.idreferencia)
				when contenido like '%%@@%%' then
				(select contenido from catalogos.autoridades a where idlink=a.idautoridad)
			else contenido end)
			, E'[\\n|\\r]+', ' ', 'g'
			)
	as contenido,
	posicion,
	f.idbiblioteca as biblioteca
FROM
	analisis.etiquetasmarc et
	INNER JOIN analisis.fichas f ON f.idficha=et.idficha
	INNER JOIN opac.actualizaciones a ON et.idficha::varchar = a.id_fila
WHERE
	a.id=%s

"""
}



##consultar la tabla de actualizaciones



try:

    siabuc.execute("SELECT id, tabla, id_fila, operacion, fecha FROM opac.actualizaciones WHERE tabla in ('ejemplares','fichas','usuarios','etiquetas')")
    for TD in siabuc.fetchall():

        if TD[1]=='fichas':

            log.debug("******* Actualizando Ficha No: %s y su contenido asociado *******" , TD[2])

            if TD[3]=='DEL':
                try:
                    opac.execute("DELETE FROM FichasGeneral WHERE Ficha_No = %s", [TD[2]])
                except opac.Error, e:
                    log.critical("ERROR no se pudo borrar los datos asociados a Fichas -> %s ", e)
                    sys.exit (1)

                try:
                    opac.execute("DELETE FROM EjemplaresGeneral WHERE Ficha_No = %s", [TD[2]])
                except opac.Error, e:
                    log.critical("ERROR no se pudo borrar los datos asociados a Fichas Ejemplares -> %s ", e)
                    sys.exit (1)

                try:
                    opac.execute("DELETE FROM EtiquetasMarc WHERE idficha = %s", [TD[2][1:]])
                except opac.Error, e:
                    log.critical("ERROR no se pudo borrar los datos asociados a Fichas EtiquetasMarc -> %s ", e)
                    sys.exit (1)

            if TD[3]=='INS' or TD[3]=='UPD':

                try:
                    fichasCur=connSiabuc.cursor()
                    ejemplaresCur=connSiabuc.cursor()
                    etiquetasCur=connSiabuc.cursor()

                    fichasCur.execute(consultas['fichas'], [TD[2],TD[2],TD[2]])
                    if fichasCur.rowcount > 0:
                        ficha=fichasCur.fetchone()

                        #borrado de fichas y dependientes en el opac
                        try:
                            opac.execute("DELETE FROM FichasGeneral WHERE Ficha_No = %s", [ficha[1]])
                            opac.execute("DELETE FROM EjemplaresGeneral WHERE Ficha_No = %s", [ficha[1]])
                            opac.execute("DELETE FROM EtiquetasMarc WHERE idficha = %s", [ficha[1][1:]])
                        except opac.Error, e:
                            log.critical("ERROR no se pudo borrar los datos asociados a Fichas -> %s ", e)
                            sys.exit (1)

                        try:
                            opac.execute(inserts['fichas'], ficha)
                        except opac.Error, e:
                            log.critical("ERROR no se pudo ejecutar el ingreso de datos de Fichas -> %s : %s", (e.args[0], e.args[1]))
                            sys.exit (1)


                        try:
                            ejemplaresCur.execute(consultas['ejemplares'], [TD[2],TD[2]])
                            if ejemplaresCur.rowcount > 0:
                                opac.executemany(inserts['ejemplares'], ejemplaresCur.fetchall())
                            ejemplaresCur.close()
                        except opac.Error, e:
                            log.critical("ERROR no se pudo ejecutar el ingreso de datos de Ejemplares -> %s : %s", (e.args[0], e.args[1]))
                            sys.exit (1)

                        try:
                            etiquetasCur.execute(consultas['etiquetas'], [TD[2],TD[2]])
                            if etiquetasCur.rowcount > 0:
                                opac.executemany(inserts['etiquetas'], etiquetasCur.fetchall())
                            etiquetasCur.close()
                        except opac.Error, e:
                            log.critical("ERROR no se pudo ejecutar el ingreso de datos de Etiquetas -> %s : %s", (e.args[0], e.args[1]))
                            sys.exit (1)


                except pg.ProgrammingError, e:
                    log.critical("ERROR no se pudo ejecutar la insercion o actualizaci�n de fichas en siabuc -> %s ", e)
                    sys.exit (1)

        if TD[1]=='usuarios':

            log.info("******* Actualizando Usuario No: %s y su contenido asociado *******", TD[2])

            if TD[3]=='DEL':
                try:
                    opac.execute("DELETE FROM UsuariosGeneral WHERE NoCuenta = %s", [TD[2]])
                except opac.Error, e:
                    log.critical("ERROR no se pudo borrar los datos asociados a Usuarios -> %s ", e)
                    sys.exit (1)

            if TD[3]=='INS':
                try:
                    usuarioCur=connSiabuc.cursor()
                    usuarioCur.execute(consultas['usuarios'], [TD[2]])

                    if usuarioCur.rowcount > 0:
                        opac.execute(inserts['usuarios'], usuarioCur.fetchone())
                except pg.ProgrammingError, e:
                    log.critical("ERROR no se pudo ejecutar la seleccion de Bibliotecas en siabuc -> %s", e)
                    sys.exit (1)
                except opac.Error, e:
                        log.critical("ERROR no se pudo ejecutar el ingreso de datos de Usuarios -> %s ", e)
                        sys.exit (1)

        siabuc.execute("DELETE FROM opac.actualizaciones WHERE id = %s", [TD[0]])
    siabuc.close()


except pg.ProgrammingError, e:
    log.critical("ERROR no se pudo ejecutar la seleccion de Actualizaciones en siabuc -> %s ", e)
    sys.exit (1)

fin=time()
log.info("Proceso de actualizacion de acervo realizado en %s segundos ", (fin-inicio))
