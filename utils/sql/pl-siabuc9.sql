CREATE SCHEMA opac;

CREATE SEQUENCE opac.actualizacion_id_seq;

CREATE TABLE opac.actualizaciones 
(
	id BIGINT DEFAULT NEXTVAL('opac.actualizacion_id_seq') PRIMARY KEY,
	tabla VARCHAR(100),
	id_fila VARCHAR(100),
	operacion CHAR(3),
	fecha TIMESTAMP DEFAULT current_timestamp
);

-- 
-- Function: opac.act_fichas()
-- DROP FUNCTION opac.act_fichas();

CREATE OR REPLACE FUNCTION opac.act_fichas()
  RETURNS "trigger" AS
$BODY$
	BEGIN
		IF (TG_OP = 'DELETE') THEN
			IF (OLD.ficha_no>0) THEN
				INSERT INTO opac.actualizaciones (tabla,id_fila,operacion) VALUES (TG_TABLE_NAME, OLD.tipoficha || OLD.idficha, 'DEL');
			END IF;
            RETURN OLD;
        ELSIF (TG_OP = 'UPDATE') THEN
		IF (NEW.ficha_no>0) THEN
			INSERT INTO opac.actualizaciones (tabla,id_fila,operacion) VALUES (TG_TABLE_NAME, NEW.idficha, 'UPD');
		END IF;
            RETURN NEW;
        ELSIF (TG_OP = 'INSERT') THEN
		IF (NEW.ficha_no>0) THEN
			INSERT INTO opac.actualizaciones (tabla,id_fila,operacion) VALUES (TG_TABLE_NAME, NEW.idficha, 'INS');
		END IF;
            RETURN NEW;
        END IF;
        RETURN NULL; -- result is ignored since this is an AFTER trigger
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE;



--
-- Function: opac.act_usuarios()
-- DROP FUNCTION opac.act_usuarios();

CREATE OR REPLACE FUNCTION opac.act_usuarios()
  RETURNS "trigger" AS
$BODY$
	BEGIN
		IF (TG_OP = 'DELETE') THEN
            INSERT INTO opac.actualizaciones (tabla,id_fila,operacion) VALUES (TG_TABLE_NAME, OLD.no_cuenta, 'DEL');
            RETURN OLD;
        ELSIF (TG_OP = 'UPDATE') THEN
            INSERT INTO opac.actualizaciones (tabla,id_fila,operacion) VALUES (TG_TABLE_NAME, OLD.no_cuenta, 'DEL');
	    	INSERT INTO opac.actualizaciones (tabla,id_fila,operacion) VALUES (TG_TABLE_NAME, NEW.no_cuenta, 'INS');
            RETURN NEW;
        ELSIF (TG_OP = 'INSERT') THEN
            INSERT INTO opac.actualizaciones (tabla,id_fila,operacion) VALUES (TG_TABLE_NAME, NEW.no_cuenta, 'INS');
            RETURN NEW;
        END IF;
        RETURN NULL; -- result is ignored since this is an AFTER trigger
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE;

  
  --
-- Function: opac.act_bibliotecas()
-- DROP FUNCTION opac.act_bibliotecas();

CREATE OR REPLACE FUNCTION opac.act_bibliotecas()
  RETURNS "trigger" AS
$BODY$
	BEGIN
		IF (TG_OP = 'DELETE') THEN
            INSERT INTO opac.actualizaciones (tabla,id_fila,operacion) VALUES (TG_TABLE_NAME, OLD.idbiblioteca, 'DEL');
            RETURN OLD;
        ELSIF (TG_OP = 'UPDATE') THEN
            INSERT INTO opac.actualizaciones (tabla,id_fila,operacion) VALUES (TG_TABLE_NAME, OLD.idbiblioteca, 'DEL');
	    	INSERT INTO opac.actualizaciones (tabla,id_fila,operacion) VALUES (TG_TABLE_NAME, NEW.idbiblioteca, 'INS');
            RETURN NEW;
        ELSIF (TG_OP = 'INSERT') THEN
            INSERT INTO opac.actualizaciones (tabla,id_fila,operacion) VALUES (TG_TABLE_NAME, NEW.idbiblioteca, 'INS');
            RETURN NEW;
        END IF;
        RETURN NULL; -- result is ignored since this is an AFTER trigger
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE;

--
-- Function: opac.act_prestamos()
-- DROP FUNCTION opac.act_prestamos();

CREATE OR REPLACE FUNCTION opac.act_prestamos()
  RETURNS "trigger" AS
$BODY$
	BEGIN
		IF (TG_OP = 'DELETE') THEN
            INSERT INTO opac.actualizaciones (tabla,id_fila,operacion) VALUES (TG_TABLE_NAME, OLD.no_adqui || '|' || OLD.no_cuenta || '|' || OLD.idbiblioteca, 'DEL');
            RETURN OLD;
        ELSIF (TG_OP = 'UPDATE') THEN
            INSERT INTO opac.actualizaciones (tabla,id_fila,operacion) VALUES (TG_TABLE_NAME, OLD.no_adqui || '|' || OLD.no_cuenta || '|' || OLD.idbiblioteca, 'DEL');
            INSERT INTO opac.actualizaciones (tabla,id_fila,operacion) VALUES (TG_TABLE_NAME, NEW.idprestamo, 'INS');
            RETURN NEW;
        ELSIF (TG_OP = 'INSERT') THEN
            INSERT INTO opac.actualizaciones (tabla,id_fila,operacion) VALUES (TG_TABLE_NAME, NEW.idprestamo, 'INS');
            RETURN NEW;
        END IF;
        RETURN NULL; -- result is ignored since this is an AFTER trigger
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE;




--
-- Function: opac.act_multas()
-- DROP FUNCTION opac.act_multas();

CREATE OR REPLACE FUNCTION opac.act_multas()
  RETURNS "trigger" AS
$BODY$
	BEGIN
		IF (TG_OP = 'DELETE') THEN
            INSERT INTO opac.actualizaciones (tabla,id_fila,operacion) VALUES (TG_TABLE_NAME, OLD.idbloqueo, 'DEL');
            RETURN OLD;
        ELSIF (TG_OP = 'UPDATE') THEN
            INSERT INTO opac.actualizaciones (tabla,id_fila,operacion) VALUES (TG_TABLE_NAME, OLD.idbloqueo, 'DEL');
            INSERT INTO opac.actualizaciones (tabla,id_fila,operacion) VALUES (TG_TABLE_NAME, NEW.idbloqueo, 'INS');
            RETURN NEW;
        ELSIF (TG_OP = 'INSERT') THEN
            INSERT INTO opac.actualizaciones (tabla,id_fila,operacion) VALUES (TG_TABLE_NAME, NEW.idbloqueo, 'INS');
            RETURN NEW;
        END IF;
        RETURN NULL; -- result is ignored since this is an AFTER trigger
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE;




--
-- Function: opac.act_reservados()
-- DROP FUNCTION opac.act_reservados();

CREATE OR REPLACE FUNCTION opac.act_reservados()
  RETURNS "trigger" AS
$BODY$
	BEGIN
		IF (TG_OP = 'DELETE') THEN
            INSERT INTO opac.actualizaciones (tabla,id_fila,operacion) VALUES (TG_TABLE_NAME, OLD.idreservacion, 'DEL');
            RETURN OLD;
        ELSIF (TG_OP = 'UPDATE') THEN
            INSERT INTO opac.actualizaciones (tabla,id_fila,operacion) VALUES (TG_TABLE_NAME, OLD.idreservacion, 'DEL');
            INSERT INTO opac.actualizaciones (tabla,id_fila,operacion) VALUES (TG_TABLE_NAME, NEW.idreservacion, 'INS');
            RETURN NEW;
        ELSIF (TG_OP = 'INSERT') THEN
            INSERT INTO opac.actualizaciones (tabla,id_fila,operacion) VALUES (TG_TABLE_NAME, NEW.idreservacion, 'INS');
            RETURN NEW;
        END IF;
        RETURN NULL; -- result is ignored since this is an AFTER trigger
    END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE;


CREATE TRIGGER tri_act_fichas
  AFTER INSERT OR UPDATE OR DELETE
  ON analisis.fichas
  FOR EACH ROW
  EXECUTE PROCEDURE opac.act_fichas();

CREATE TRIGGER tri_act_usuarios
  AFTER INSERT OR UPDATE OR DELETE
  ON catalogos.usuarios
  FOR EACH ROW
  EXECUTE PROCEDURE opac.act_usuarios();

CREATE TRIGGER tri_act_bibliotecas
  AFTER INSERT OR UPDATE OR DELETE
  ON catalogos.bibliotecas
  FOR EACH ROW
  EXECUTE PROCEDURE opac.act_bibliotecas();

CREATE TRIGGER tri_act_prestamos
  AFTER INSERT OR UPDATE OR DELETE
  ON prestamo.prestamos
  FOR EACH ROW
  EXECUTE PROCEDURE opac.act_prestamos();

CREATE TRIGGER tri_act_multas
  AFTER INSERT OR UPDATE OR DELETE
  ON prestamo.bloqueos
  FOR EACH ROW 
  EXECUTE PROCEDURE opac.act_multas();

CREATE TRIGGER tri_act_reservados
  AFTER INSERT OR UPDATE OR DELETE
  ON prestamo.reservaciones
  FOR EACH ROW
  EXECUTE PROCEDURE opac.act_reservados();
  
-- Permisos Schema OPAC
GRANT ALL ON SCHEMA opac TO s9_admin;
GRANT USAGE ON SCHEMA opac TO s9_super;
GRANT USAGE ON SCHEMA opac TO s9_usuario1;
GRANT USAGE ON SCHEMA opac TO s9_usuario2;

-- Permisos tabla opac.actualizaciones
GRANT ALL ON TABLE opac.actualizaciones TO s9_super;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE opac.actualizaciones TO s9_admin;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE opac.actualizaciones TO s9_usuario1;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE opac.actualizaciones TO s9_usuario2;

-- Permisos secuencia opac.actualizacion_id_seq

GRANT ALL ON TABLE opac.actualizacion_id_seq TO s9_super;
GRANT SELECT, UPDATE ON TABLE opac.actualizacion_id_seq TO s9_admin;
GRANT SELECT, UPDATE ON TABLE opac.actualizacion_id_seq TO s9_usuario1;
GRANT SELECT, UPDATE ON TABLE opac.actualizacion_id_seq TO s9_usuario2;
