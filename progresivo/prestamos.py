# -*- coding: utf-8 -*-
import sys, ConfigParser as cfgparser, os, MySQLdb as mysql, psycopg2 as pg, re, types, datetime, logging as log, warnings
from decimal import Decimal
from time import time

config=cfgparser.RawConfigParser()
config.read('../config.ini')

log.basicConfig(format='%(asctime)s -> %(levelname)s: %(message)s',filename='cliente-siabuc9.log',level=log.INFO)
log.captureWarnings(True)


inicio=time()
log.debug("******** iniciando proceso de prestamos ***********")



try:
    connSiabuc = pg.connect(host=config.get('hostsiabuc','host'), user=config.get('hostsiabuc','user'), password=config.get('hostsiabuc','password'), database=config.get('hostsiabuc','database'), port=int(config.get('hostsiabuc','port')))
    connSiabuc.set_client_encoding(config.get('hostsiabuc','codificacion'))
    siabuc=connSiabuc.cursor()
except pg.InternalError, e:
  log.critical("ERROR en la conexion con siabuc -> %s " % e)
  sys.exit (1)

try:
    db=mysql.connect(host=config.get('hostopac','host'), user=config.get('hostopac','user'),passwd=config.get('hostopac','password'),db=config.get('hostopac','database'),port=int(config.get('hostopac','port')))
    db.set_character_set('utf8')
    opac=db.cursor()
except mysql.Error, e:
    log.critical("Error en la conexion con el opac -> %s: %s", (e.args[0], e.args[1]))
    sys.exit (1)

## definición de consultas

inserts={
"prestamos":""" 
REPLACE INTO 
	PrestamosGeneral 
VALUES (
	%s,
	%s,
	%s,
	%s,
	%s,
	%s,
	%s,
	%s,
	%s,
	%s
)
""",
"reservados":"""
REPLACE INTO 
Reservados (
	numadqui, 
	NoCuenta, 
	fechainicial, 
	fechafinal,
	Biblioteca
) 
VALUES (
	%s,
	%s,
	%s,
	%s,
	%s
	)
""",
"multas":"""
REPLACE INTO 
MultasPendientes (
	IdMulta, 
	NoCuenta, 
	Nombre, 
	Escuela, 
	FechaDevolucion, 
	Monto, 
	Observaciones, 
	idCapturista, 
	idBloqueo,
	Biblioteca
) 
VALUES (
	%s,
	%s,
	%s,
	%s,
	%s,
	%s,
	%s,
	%s,
	%s,
	%s
)
"""
}

consultas={
"prestamos": """
SELECT 
	no_cuenta,
	no_adqui,
	usuario,
	titulo,
	autor,
	clasificacion,
	f_prestamo AS fecha_salida,
	f_entrega AS fecha_entrega,
	'Prestamo' AS tipo_prestamo,
	idbiblioteca AS biblioteca
FROM
	prestamo.prestamos p 
	inner join opac.actualizaciones a on p.idprestamo::varchar = a.id_fila
WHERE
	tipo_prestamo<>'PS'
	AND a.id=%s
	
UNION

SELECT
	no_cuenta,
	no_adqui,
	usuario,
	titulo,
	autor,
	clasificacion,
	NULL AS fecha_salida,
	NULL AS fecha_entrega,
	'Prestamo interno' AS tipo_prestamo,
	idbiblioteca AS biblioteca
FROM
	prestamo.prestamos p 
	inner join opac.actualizaciones a on p.idprestamo::varchar = a.id_fila
WHERE
	tipo_prestamo='PS'
	AND a.id=%s
	
""",
"reservados": """ 
SELECT 
	no_adqui,
	fecha_inicio,
	fecha_fin,
	no_cuenta,
	idbiblioteca AS biblioteca
FROM 
	prestamo.reservaciones r 
	inner join opac.actualizaciones a on r.idreservacion::varchar = a.id_fila
WHERE 
	activa=true	
	AND a.id=%s
""",
"multas": """
SELECT 
	pb.idbloqueo as idbloqueo,
	pb.no_cuenta as no_cuenta,
	cu.nombre as nombre,
	es.nombre as escuela,
	pb.f_devolucion as f_devolucion,
	pb.monto as monto,
	pb.observaciones as observaciones,
	pb.analista as analista,
	pb.idbloqueo as idbloqueo,
	es.idbiblioteca as biblioteca
FROM 
	prestamo.bloqueos pb 
	inner join opac.actualizaciones a on pb.idbloqueo::varchar = a.id_fila
	inner join catalogos.usuarios cu on pb.no_cuenta = cu.no_cuenta
	inner join catalogos.escuelas es on pb.idescuela = es.idescuela
WHERE 
	a.id=%s
"""


}

##consultar la tabla de actualizaciones

try:
    siabuc.execute("SELECT id, tabla, id_fila, operacion, fecha FROM opac.actualizaciones WHERE tabla in ('prestamos','reservaciones','bloqueos')")
    for TD in siabuc.fetchall():
        
        if TD[1]=='prestamos':
            log.debug("******* Actualizando Prestamos No: %s y su contenido asociado *******",TD[2])
            
            if TD[3]=='DEL':
                try:
                    opac.execute("DELETE FROM PrestamosGeneral WHERE NumAdqui = %s AND NoCuenta = %s AND Biblioteca = %s", [TD[2].split('|')])
                except opac.Error, e:
                    log.critical("ERROR no se pudo borrar los datos asociados a Prestamos -> %s " % e)
                    log.debug(opac.query)
                    sys.exit (1)
            
            if TD[3]=='INS':
                
                try:
                    siabuc.execute(consultas['prestamos'],[TD[2],TD[2]])
                    if siabuc.rowcount > 0:
                        opac.execute(inserts['prestamos'],siabuc.fetchone())
                        
                except pg.ProgrammingError, e:
                    log.critical("ERROR no se pudo ejecutar la seleccion de Ejemplares en siabuc -> %s ", e)
                    sys.exit (1)
                except opac.Error, e:
                    log.critical("ERROR no se pudo ejecutar el ingreso de datos de Prestamos -> %s : %s", (e.args[0], e.args[1]))
                    sys.exit (1)
                
                
        if TD[1]=='reservaciones':
            log.debug( "******* Actualizando Reservados No: %s y su contenido asociado *******" , TD[2])
            
            if TD[3]=='DEL':
            
                try:
                    opac.execute("DELETE FROM Reservados WHERE NumAdqui = %s", [TD[2]])
                except opac.Error, e:
                    log.critical("ERROR no se pudo borrar los datos asociados a Reservados -> %s ", e)
                    sys.exit (1)
                
            if TD[3]=='INS':
                try:
                    siabuc.execute(consultas['reservados'], [TD[2]])
                    if siabuc.rowcount > 0:
                        opac.execute(inserts['reservados'], siabuc.fetchone())
                
                except pg.ProgrammingError, e:
                    log.critical("ERROR no se pudo ejecutar la seleccion de Reservados en siabuc -> %s ", e)
                    sys.exit (1)
                except opac.Error, e:
                    log.critical("ERROR no se pudo ejecutar el ingreso de datos de Reservados -> %s : %s", (e.args[0], e.args[1]))
                    sys.exit (1)
                
        if TD[1]=='bloqueos':
            log.debug("******* Actualizando Multas No: %s y su contenido asociado *******", TD[2])
            
            if TD[3]=='DEL':
            
                try:
                    opac.execute("DELETE FROM MultasPendientes WHERE IdBloqueo = %s", [TD[2]]) 
                except opac.Error, e:
                    log.critical("ERROR no se pudo borrar los datos asociados a Multas -> %s ", e)
                    sys.exit (1)
                
            if TD[3]=='INS':
                
                try:
                    siabuc.execute(consultas['multas'], [TD[2]])
                    if siabuc.rowcount > 0:
                        opac.execute(inserts['multas'], siabuc.fetchone())
                        
                except pg.ProgrammingError, e:
                    log.critical("ERROR no se pudo ejecutar la seleccion de Multas en siabuc -> %s ", e)
                    sys.exit (1)
                except opac.Error, e:
                    log.critical("ERROR no se pudo ejecutar el ingreso de datos de Multas -> %s : %s", (e.args[0], e.args[1]))
                    sys.exit (1)
        
        siabuc.execute("DELETE FROM opac.actualizaciones WHERE id = %s", [TD[0]])
        

except pg.ProgrammingError, e:
    log.critical("ERROR no se pudo ejecutar la seleccion de Actualizaciones en siabuc -> %s ", e)
    sys.exit (1)

fin=time()
log.info("Proceso de actualizacion de prestamos realizado en %s segundos ", (fin-inicio))
